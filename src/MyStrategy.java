// Copyright (C) Maxime MORGE 2021
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import genius.core.Agent;
import genius.core.Bid;
import genius.core.NegotiationResult;
import genius.core.actions.*;
import genius.core.issue.*;

/**
 *  My strategy chooses randomly a bid better than its reservation value
 *  and accepts the last bid of the opponent if the current bid is not better
 */
public class MyStrategy extends Agent {

    private final boolean debug = true;
    private static final long serialVersionUID = 1L;

    /**
     * Human readable strategy description
     */
    public String getDescription() {
        return "My strategy";
    }

    /**
     * Reservation value
     */
    private static double RESERVATION_VALUE = 0.0;

    /**
     * Method called when a new session starts
     */
    @Override
    public void init() {
        RESERVATION_VALUE = utilitySpace.getReservationValueUndiscounted();
    }

    /**
     * Method which informs the agent about its utility
     */
    @Override
    public void endSession(NegotiationResult result) {
        if (debug) System.out.println("Agent "+getAgentID()+" utility: " + result.getMyDiscountedUtility());
    }

    /**
     * The last action of the opponent
     */
    private Action lastOpponentAction = null;


    /**
     * Assign the last opponent action
     */
    public void ReceiveMessage(Action opponentAction) {
        lastOpponentAction = opponentAction;
    }

    /**
     * Compute the next action
     */
    @Override
    public Action chooseAction() {
        Action action = null;
        Bid lastOpponentBid = null;
        try {
            if (lastOpponentAction == null)
                action = chooseRandomBidAction();
            if (lastOpponentAction instanceof Offer) {
                // Look after the last opponent bid
                lastOpponentBid = ((Offer) lastOpponentAction).getBid();
                double utilityOfLastOpponentBid = getUtility(lastOpponentBid);

                // Choose a random bid
                action = chooseRandomBidAction();
                Bid nextBid = ((Offer) action).getBid();
                double utilityOfNextBid = getUtility(nextBid);

                // Accept the last bid of the opponent if my bid is not better
                if (utilityOfLastOpponentBid > utilityOfNextBid)
                    action = new Accept(getAgentID(), lastOpponentBid);
            }
        } catch (Exception e) {
            System.out.println("MyStrategy exception in chooseAction: " + e.getMessage());
            action = new Accept(getAgentID(), lastOpponentBid); // best guess if things go wrong.
        }
        return action;
    }


    /**
     * Convenient wrapper for getRandomBid
     * which returns an acceptance if an exception is raised
     */
    private Action chooseRandomBidAction() {
        Bid nextBid = null;
        try {
            nextBid = getRandomBid();
            if (debug) System.out.println("Agent "+getAgentID()+" randomly chooses the bid: " + nextBid);
        } catch (Exception e) {
            System.out.println("MyStrategy exception in chooseRandomBid:" + e.getMessage());
        }
        if (nextBid == null)
            return new Accept(getAgentID(),	((ActionWithBid) lastOpponentAction).getBid());
        return new Offer(getAgentID(), nextBid);
    }


    /**
     * Create a random bid better than the reservation value
     */
    private Bid getRandomBid() throws Exception {
        HashMap<Integer, Value> values = new HashMap<>();
        // pairs of issue number, value
        List<Issue> issues = utilitySpace.getDomain().getIssues();
        Random randomGenerator = new Random();
        Bid bid;
        do {
            for (Issue issue : issues) {
                switch (issue.getType()) {
                    case DISCRETE :
                        IssueDiscrete discreteIssue = (IssueDiscrete) issue;
                        int indexValue = randomGenerator
                                .nextInt(discreteIssue.getNumberOfValues());
                        values.put(issue.getNumber(),
                                discreteIssue.getValue(indexValue));
                        break;
                    default :
                        throw new Exception("MyStrategy exception in getRandomBid: " +
                                issue.getType() + " is not supported by my strategy");
                }
            }
            bid = new Bid(utilitySpace.getDomain(), values);
        } while (getUtility(bid) < RESERVATION_VALUE);
        return bid;
    }
}